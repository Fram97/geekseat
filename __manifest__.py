# -*- coding: utf-8 -*-
{
    'name': "Febry Ramadhan - Geekseat",

    'summary': """
        Geekseat Test""",

    'description': """
        Geekseat
    """,

    'author': "Febry Ramadhan",
    'website': "framad.my.id",

    'category': 'Uncategorized',
    'version': '0.1',

    # Depencicy
    'depends': [
        'base',
        'sale'
    ],

    # Include ALL XML Code in Here be mindful of order
    'data': [
        'security/access_groups.xml',
        'security/record_rules.xml',
        'security/ir.model.access.csv',
        'views/sale.views.xml',
    ],

    'installable': True,
    'application': True,
    'auto_install': False

}
