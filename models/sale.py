from odoo import models, fields, exceptions, api

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    custom_field = fields.Char(string='Custom Field')
    company_id = fields.Many2one(comodel_name='res.company', string="Company")

    @api.constrains('order_line')
    def _check_lines_unique_product(self):
        for record in self:
            product_ids = set()
            for line in record.order_line:
                if line.product_id.id in product_ids:
                    raise exceptions.ValidationError("Tidak bisa memilih produk yang sama sebayak lebih dari 2 kali")
                product_ids.add(line.product_id.id)